#!/bin/sh

# Install from pip since they are missing in RHEL
pip install --ignore-installed pylint nose

# Run test
pushd source
make test
make unittest
popd
